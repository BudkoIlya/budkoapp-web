import React from 'react'
import ReactDOM from 'react-dom'
import thunk from 'redux-thunk'
import './index.css'
import * as serviceWorker from './serviceWorker'
import { Provider } from 'react-redux'
import App from './App'
import { BrowserRouter } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css'
import store from './reducers/store'
import { PersistGate } from 'redux-persist/integration/react'

ReactDOM.render(
    <Provider store={store.store}>
        <PersistGate loading={null} persistor={store.persistor}>
            <BrowserRouter>
                <App />
            </BrowserRouter>
        </PersistGate>
    </Provider>,
    document.getElementById('root')
)

serviceWorker.unregister()
