import React from 'react'
import { Field } from 'redux-form'
import { Input } from 'reactstrap'

const WrappedTextInput = ({ input, meta: { touched, error }, ...props }) => {
    // console.log('err',error)
    return (
        <div>
            {/*в инпут передаётся onFocus и onChange, в пропсах передаётся параметры типа placholder, type, name*/}
            <Input {...input} {...props} />
            {/*Если импут не нажат или есть ошибка валидации, то выдаёт ошибку*/}
            {touched && error && <p className="text-warning">{error}</p>}
        </div>
    )
}

const TextInput = props => <Field component={WrappedTextInput} {...props} />

export default TextInput
