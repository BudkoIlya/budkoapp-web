import React from 'react'
import { reduxForm } from 'redux-form'
import TextInput from '../formInput/TextInput'
import { email, required, password, passwords } from '../validate/validate'
import NetworksContainer from '../networks/networksContainer'
import { NavLink, withRouter } from 'react-router-dom'
import { Button, Label } from 'reactstrap'

const SignUp = props => {
    return (
        <div className="w-25 m-auto">
            <form onSubmit={props.handleSubmit(props.register)}>
                <h2 className="text-center">Регистрация</h2>
                <div>
                    <Label>Имя</Label>
                    <div>
                        <TextInput
                            name="name"
                            type="text"
                            placeholder="Name"
                            className="email"
                            validate={[required]}
                        />
                    </div>
                </div>
                <div>
                    <Label>Фамилия</Label>
                    <div>
                        <TextInput
                            name="lastName"
                            type="text"
                            placeholder="LastName"
                            className="email"
                        />
                    </div>
                </div>
                <div>
                    <Label>Email</Label>
                    <div>
                        <TextInput
                            name="email"
                            type="text"
                            placeholder="Email"
                            className="email"
                            validate={[email, required]}
                        />
                    </div>
                    {props.err !== '' ? (
                        <div>Такой email уже существует!</div>
                    ) : null}
                </div>
                <div>
                    <Label>Пароль</Label>
                    <div>
                        <TextInput
                            name="password"
                            type="text"
                            placeholder="Password"
                            className="password"
                            validate={[password, required]}
                        />
                    </div>
                </div>
                <div>
                    <Label>Подтвердить пароль</Label>
                    <div>
                        <TextInput
                            name="passwordConfirm"
                            type="text"
                            placeholder="Password"
                            className="password"
                            validate={[passwords, required]}
                        />
                    </div>
                </div>

                <Button color="primary" className="w-100 my-2">
                    Регистрация
                </Button>
            </form>
            <NetworksContainer />
            <div className="d-flex justify-content-between mb-1">
                <NavLink to="/">Войти</NavLink>
                <NavLink to="/restorepass">Забыли пароль?</NavLink>
            </div>
        </div>
    )
}

export default reduxForm({
    form: 'simple', // a unique identifier for this form
})(withRouter(SignUp))
