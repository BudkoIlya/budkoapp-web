import React, { Component } from 'react'
import { connect } from 'react-redux'
import SignUp from './signUp'
import { errSignCreator, signInCreator } from '../../reducers/users-reducer'
import { toast } from 'react-toastify'

class SignUpContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            err: '',
        }
    }
    notifyRegister = () => {
        toast.success('Вы зарегистрировались!')
    }

    register = data => {
        console.log('Локальная авторизация', data)
        let req = {
            user: {
                name: data.name,
                lastName: data.lastName,
                email: data.email,
            },
            password: data.password,
        }
        console.log(req)
        fetch('http://localhost:3001/register', {
            method: 'POST',
            headers: {
                Accept: '*/*',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(req),
        })
            .then(res => res.json())
            .then(res => {
                console.log('Локальная регистрация прошла', res)
                if (res.error) {
                    this.setState({ err: res.error })
                } else {
                    this.setState({ err: '' })
                    // перенаправляет на нужную нам страницу
                    this.notifyRegister()
                    this.props.history.push('/')
                    // this.props.changePageOnSignUp()
                }
            })
    }

    render() {
        return (
            <SignUp
                register={this.register}
                signIn={this.props.signIn}
                // signUp={this.props.signUp}
                errSign={this.props.errSign}
                err={this.state.err}
            />
        )
    }
}

let mapStateToProps = ({ users }) => ({ users })
let mapDispatchToProps = dispatch => {
    return {
        // changePageOnSignIn: () => {
        //     dispatch(changePageOnSignInCR())
        // },
        // changePageOnSignUp: () => {
        //     dispatch(changePageOnSignUpCR())
        // },
        signIn: user => dispatch(signInCreator(user)),
        errSign: err => {
            dispatch(errSignCreator(err))
        },
        // signUp: user => {
        //     dispatch(signUpCreator(user))
        // },
    }
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SignUpContainer)
