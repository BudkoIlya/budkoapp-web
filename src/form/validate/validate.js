const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
// const PASS_REGEX = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;

let passwordOne = ''

export const required = value => !value && 'Пожалуйста, заполните поле'
export const policy = value => !value && 'Вы должны принять уловия'
export const email = value =>
    //test() - выполняет поиск совпадения между регулярным выражением и определенной строкой
    value &&
    !EMAIL_REGEX.test(value.toLowerCase()) &&
    'Введен некорректный email'
export const password = value => {
    passwordOne = value
    if (!value) {
        return 'Это поле обязательно'
    } else if (
        !/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]{8,16}$/.test(value)
    ) {
        if (!/^(?=.*[a-z])(?=.*[A-Z])/.test(value))
            return 'Пароль должен содержать буквы в верхнем и нижнем регистре'
        if (!/^(?=.*[0-9])/.test(value.toLowerCase()))
            return 'Пароль должен содержать хотя бы одну цифру'
        if (!/^[a-zA-Z0-9]{8,16}$/.test(value))
            return 'Пароль должен быть не менее 8 и не более 16 символов'
    }
}
export const passwords = value => {
    // console.log(value, passwordOne)
    return value !== passwordOne && 'Пароли должны совпадать'
}
