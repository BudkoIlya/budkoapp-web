import React from 'react'
import VkLogin from 'react-vk-auth'
import GoogleLogin from 'react-google-login'
import VkLogo from '../../img/vk-logo.png'
const Networks = props => {
    // debugger;
    return (
        <div className="d-flex justify-content-between">
            <div className="mb-1">
                <div className="h-100">
                    <VkLogin
                        className="bg-light border rounded h-100 "
                        apiId={process.env.REACT_APP_VK_CLIENT_ID}
                        fields={VkLogo}
                        callback={props.responseVK}
                    >
                        <div className="d-flex h-100">
                            <div className="mr-3 d-flex align-items-center">
                                <img
                                    className="img mx-auto"
                                    src={VkLogo}
                                    alt=""
                                />
                            </div>
                            <div className="d-flex align-items-center text-secondary">
                                Login
                            </div>
                        </div>
                    </VkLogin>
                </div>
            </div>
            <div className="mb-1">
                <GoogleLogin
                    className="shadow-none border rounded text-secondary"
                    clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
                    buttonText="Login"
                    onSuccess={props.responseGoogle}
                    // onFailure={console.log('asdad')}
                    cookiePolicy={'single_host_origin'}
                />
            </div>
        </div>
    )
}

export default Networks
