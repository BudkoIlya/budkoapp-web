import React, { Component } from 'react'
import { connect } from 'react-redux'
import Networks from './networks'
import {
    errSignCreator,
    setUsersCreator,
    signInCreator,
    // signUpCreator,
} from '../../reducers/users-reducer'
import { withRouter } from 'react-router-dom'

class NetworksContainer extends Component {
    responseVK = async data => {
        console.log('Авторизация через VK', data)
        if (data.session) {
            const token = new Blob(
                [JSON.stringify({ access_token: data.session.sid }, null, 2)],
                { type: 'application/json' }
            )
            fetch('http://localhost:3001/auth/vkontakte', {
                method: 'POST',
                mode: 'cors',
                cache: 'default',
                headers: {
                    'Access-Control-Allow-Origin': '*',
                },
                body: token,
            })
                .then(res => res.json())
                .then(user => {
                    if (user.firstName) {
                        console.log(
                            'Ответ от VK если пользователь есть в базе',
                            user
                        )
                        // с помощью вк не получается получить мыло, поэтому отображаю имя
                        const person = {
                            email: user.firstName,
                            JWToken: user.JWToken,
                        }
                        this.props.signIn(person)
                        this.props.history.push('/dialogs')
                    } else {
                        console.log('Не удалось войти')
                        this.props.errSign('err')
                    }
                })
        }
    }
    responseGoogle = async data => {
        console.log('Авторизация через Google', data)
        const token = new Blob(
            [JSON.stringify({ access_token: data.accessToken }, null, 2)],
            { type: 'application/json' }
        )
        fetch('http://localhost:3001/auth/google', {
            method: 'POST',
            mode: 'cors',
            cache: 'default',
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
            body: token,
        })
            .then(res => res.json())
            .then(user => {
                if (user.email) {
                    console.log(
                        'Ответ от Google если пользователь есть в базе',
                        user
                    )
                    this.props.signIn(user)
                    this.props.history.push('/dialogs')
                } else {
                    console.log('Не удалось войти')
                    this.props.errSign('err')
                }
            })
    }

    render() {
        return (
            <Networks
                signIn={this.props.signIn}
                // signUp={this.props.signUp}
                errSign={this.props.errSign}
                responseVK={this.responseVK}
                responseGoogle={this.responseGoogle}
            />
        )
    }
}

let mapStateToProps = ({ users }) => ({
    users,
})

let mapDispatchToProps = dispatch => {
    return {
        signIn: user => dispatch(signInCreator(user)),
        errSign: err => {
            dispatch(errSignCreator(err))
        },
        // signUp: user => {
        //     dispatch(signUpCreator(user))
        // },
        setUsers: users => {
            dispatch(setUsersCreator(users))
        },
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withRouter(NetworksContainer))
