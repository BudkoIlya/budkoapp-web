import React from 'react'
import { reduxForm } from 'redux-form'
import TextInput from '../formInput/TextInput'
import { email, required, password, passwords } from '../validate/validate'
import { ToastContainer } from 'react-toastify'
import { NavLink } from 'react-router-dom'
import { Button, Form, Label } from 'reactstrap'

const RestorePass = props => {
    return (
        <div className="w-25 m-auto">
            {!props.state.token ? (
                <Form onSubmit={props.handleSubmit(props.restPass)}>
                    <h2 className="text-center">Восстановление пароля</h2>
                    <div>
                        <Label>Email</Label>
                        <div>
                            <TextInput
                                name="email"
                                type="text"
                                placeholder="Email"
                                className="email"
                                validate={[email, required]}
                            />
                        </div>
                    </div>
                    <Button color="primary" className="w-100 my-2">
                        Восстановить пароль
                    </Button>
                </Form>
            ) : (
                // вторая форма, если пользователь перешёл по ссылке полученной по почте и получил токен
                <Form onSubmit={props.handleSubmit(props.changePass)}>
                    <h2>Восстановление пароля</h2>
                    <div>
                        <Label>Новый пароль</Label>
                        <div>
                            <TextInput
                                name="password"
                                type="text"
                                placeholder="New password"
                                className="password"
                                validate={[password, required]}
                            />
                        </div>
                        <div>
                            <TextInput
                                name="newPassword"
                                type="text"
                                placeholder="Confirm password"
                                className="password"
                                validate={[passwords, required]}
                            />
                        </div>
                    </div>
                    {props.state.err === true ? (
                        <div>Ошибка смены пароля</div>
                    ) : null}
                    <Button color="primary" className="w-100 my-2">
                        Сохранить новый пароль
                    </Button>
                </Form>
            )}
            <ToastContainer
                position="top-center"
                autoClose={4000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnVisibilityChange
                draggable
                pauseOnHover
            />
            <div className="d-flex justify-content-between mb-1">
                <NavLink to="/">Войти</NavLink>
                <NavLink to="/signup">Регистрация</NavLink>
            </div>
        </div>
    )
}

export default reduxForm({
    form: 'simple', // a unique identifier for this form
})(RestorePass)
