import React, { Component } from 'react'
import RestorePass from './restorePass'
import queryString from 'query-string'
import { withRouter } from 'react-router-dom'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

class RestorePassContainer extends Component {
    constructor(props) {
        super(props)
        console.log(props)
        this.state = {
            restorePass: null,
            // ниже отслеживаем url строку и извлекаем из неё токен и имайл
            token: queryString.parse(this.props.location.search).token,
            email: queryString.parse(this.props.location.search).email,
            err: false,
        }
    }
    componentDidMount() {
        console.log(this.state.token)
        console.log('email', this.state.email)
        console.log(this.state)
    }

    restPass = data => {
        console.log('отправляемый email для вост.пароля', data)
        fetch('http://localhost:3001/restorepass', {
            method: 'POST',
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        })
            .then(res => res.json())
            .then(res => {
                if (res.password) {
                    this.notifySuccess()
                } else {
                    this.notifyError()
                }
            })
    }
    changePass = data => {
        const user = {
            token: this.state.token,
            email: this.state.email,
            newPassword: data.newPassword,
        }
        console.log(user)
        fetch('http://localhost:3001/changepass', {
            method: 'POST',
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(user),
        })
            .then(res => res.json())
            .then(res => {
                if (res.success === true) {
                    // console.log(res.success)
                    this.setState({ err: false })
                    this.props.history.push('/')
                } else {
                    // console.log(res.success)
                    this.setState({ err: true })
                }
            })
    }
    notifySuccess = () => {
        toast.success('Пароль отправлен на вашу почту')
        this.setState({ restorePass: true })
    }
    notifyError = () => {
        toast.error("Такого email'a не существует")
        this.setState({ restorePass: false })
    }
    render() {
        return (
            <RestorePass
                restPass={this.restPass}
                changePass={this.changePass}
                state={this.state}
            />
        )
    }
}

export default withRouter(RestorePassContainer)
