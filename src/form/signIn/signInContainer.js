import React, { Component } from 'react'
import { connect } from 'react-redux'
import SignIn from './signIn'
import {
    signInCreator,
    errSignCreator,
    setUsersCreator,
} from '../../reducers/users-reducer'
import { toast } from 'react-toastify'

class SignInContainer extends Component {
    // constructor(props) {
    //     debugger;
    //     super(props);
    //     // if (!this.props.users.length) {
    //     //     fetch('http://localhost:3001/get/users', {
    //     //         method: 'POST',
    //     //         mode: "cors",
    //     //         cache: "default",
    //     //         headers: {
    //     //             "Access-Control-Allow-Origin": "*"
    //     //         }
    //     //     }).then(res => res.json()).then(users => {
    //     //             // console.log(res);
    //     //             this.props.setUsers(users)
    //     //         }
    //     //     )
    //     // }
    // }

    sendData = data => {
        console.log('Локальная авторизация', data)
        fetch('http://localhost:3001/login', {
            method: 'POST',
            headers: {
                Accept: '*/*',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
            //
        })
            .then(res => res.json())
            .then(res => {
                console.log('Локальная авторизаци прошла', res)
                console.log(res[0])
                if (res.user) {
                    // this.props.history.push('/dialogs')
                    this.props.signIn(res.user)
                    // localStorage.setItem('Token', res.user.JWToken)
                } else if (res.error[0] === 'password') {
                    this.notifyErrPass()
                    console.log('Не правильный пароль')
                    this.props.errSign('err')
                } else if (res.error[0] === 'email') {
                    console.log('Не правильный email')
                    this.notifyErrEmail()
                    this.props.errSign('err')
                }
            })
    }
    notifyErrPass = () => toast.error('Не правильный пароль!')
    notifyErrEmail = () => toast.error('Такого пользователя не существует!')

    render() {
        return (
            <SignIn
                sendData={this.sendData}
                errSign={this.props.errSign}
                // signUp={this.props.signUp}
                signIn={this.props.signIn}
                users={this.props.users}
            />
        )
    }
}

let mapStateToProps = ({ users }) => ({
    users,
})

let mapDispatchToProps = dispatch => {
    return {
        signIn: user => dispatch(signInCreator(user)),
        errSign: err => {
            dispatch(errSignCreator(err))
        },
        // signUp: user => {
        //     dispatch(signUpCreator(user))
        // },
        setUsers: users => {
            dispatch(setUsersCreator(users))
        },
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SignInContainer)
