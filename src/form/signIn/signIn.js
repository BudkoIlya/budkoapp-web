import React from 'react'
import { reduxForm } from 'redux-form'
import TextInput from '../formInput/TextInput'
import { email, required, password } from '../validate/validate'
import NetworksContainer from '../networks/networksContainer'
import { Link, withRouter } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { Button, Form, Label } from 'reactstrap'

const SignIn = props => {
    return (
        <div className="w-25 m-auto">
            <Form onSubmit={props.handleSubmit(props.sendData)}>
                <h2 className="text-center">Авторизация</h2>
                <div>
                    <Label>Email</Label>
                    <div>
                        <TextInput
                            name="email"
                            type="text"
                            placeholder="Email"
                            className="email"
                            validate={[email, required]}
                        />
                    </div>
                </div>
                <div>
                    <Label>Password</Label>
                    <div>
                        <TextInput
                            name="password"
                            type="text"
                            placeholder="Password"
                            className="password"
                            validate={[password, required]}
                        />
                    </div>
                </div>
                <div> </div>
                <Button color="primary" className="w-100 my-2">
                    Войти
                </Button>
            </Form>
            <ToastContainer
                position="top-center"
                autoClose={4000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnVisibilityChange
                draggable
                pauseOnHover
            />
            <NetworksContainer />
            <div className="d-flex justify-content-between mb-1">
                <Link to="/signup">Регистрация</Link>
                <Link to="/restorepass">Забыли пароль?</Link>
            </div>
        </div>
    )
}

export default reduxForm({
    form: 'simple', // a unique identifier for this form
})(withRouter(SignIn))
