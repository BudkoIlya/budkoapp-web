import React, { Component } from 'react'
import './App.css'
import SignUpContainer from './form/signUp/signUpContainer'
import { Switch, Route, Redirect } from 'react-router-dom'
import SignInContainer from './form/signIn/signInContainer'
import RestorePassContainer from './form/restorePass/restorePassContainer'
import { connect } from 'react-redux'
import { signOutCreator } from './reducers/users-reducer'
import DialogsContainer from './dialogs/dialogsContainer'
import { Button } from 'reactstrap'
// import { initializeFirebase } from './push-notification/push-notification'

let mapStateToProps = ({ users, dialogs }) => ({
    users,
    dialogs,
})
let mapDispatchToProps = dispatch => {
    return {
        signOut: () => {
            dispatch(signOutCreator())
        },
    }
}

class App extends Component {
    getState = () => {
        console.log('state', this.props)
        // debugger;
    }

    render() {
        return (
            <div>
                <div>
                    <Switch>
                        {/*Войти или на главную если пользователь уже вошёл*/}
                        <Route
                            path="/"
                            exact
                            render={() =>
                                !this.props.users.status ? (
                                    <SignInContainer />
                                ) : (
                                    <Redirect to="/dialogs" />
                                )
                            }
                        />
                        {/*Регистрация*/}
                        <Route path="/signup" component={SignUpContainer} />
                        {/*Востоновление пароля*/}
                        <Route
                            path="/restorepass"
                            render={() => <RestorePassContainer />}
                        />
                        {/*Главная*/}
                        <Route
                            path="/dialogs"
                            render={() => (
                                <DialogsContainer
                                    status={this.props.users.status}
                                />
                            )}
                        />
                    </Switch>
                </div>
                <Button onClick={this.getState}>State</Button>
            </div>
        )
    }
}
// Запуск уведомления (Не работает)
// initializeFirebase()

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App)
