import firebase from 'firebase'

export const initializeFirebase = () => {
    firebase.initializeApp({
        messagingSenderId: '618328005385',
    })
}

navigator.serviceWorker
    .register('/firebase-messaging-sw.js')
    .then(notification => {
        console.log('регистрация')
        firebase.messaging().useServiceWorker(notification)
    })
