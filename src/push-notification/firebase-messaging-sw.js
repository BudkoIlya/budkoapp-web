import firebase from 'firebase'
importScripts('https://www.gstatic.com/firebasejs/6.1.1/firebase-app.js')
importScripts('https://www.gstatic.com/firebasejs/6.1.1/firebase-messaging.js')

firebase.initializeApp({
    messagingSenderId: '618328005385',
})

const messaging = firebase.messaging()
