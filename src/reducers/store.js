import { applyMiddleware, combineReducers, createStore } from 'redux'
import { reducer } from 'redux-form'
import users from './users-reducer'
import dialogs from './dialogs-reducer'
import thunk from 'redux-thunk'
import storage from 'redux-persist/lib/storage'
import { persistStore, persistReducer } from 'redux-persist'

// В этомобъекте хранятся все редьюсеры
const reducers = combineReducers({
    form: reducer,
    users,
    dialogs,
})

const persistConfig = {
    key: 'users',
    storage,
    whitelist: ['users'],
}

const persistedReducer = persistReducer(persistConfig, reducers)

const store = createStore(persistedReducer, applyMiddleware(thunk))
const persistor = persistStore(store)

export default {
    store,
    persistor,
}
