const GET_DIALOGS = 'GET_DIALOGS'
const GET_MESSAGE = 'GET_MESSAGE'
const CHANGE_ON_ACTIVE = 'CHANGE_ON_ACTIVE'
const CHANGE_ON_SAVED = 'CHANGE_ON_SAVED'
const GET_DIALOGS_PIECE = 'GET_DIALOGS_PIECE'
// const GET_USERS = 'GET_USERS'

export const getDialogsCR = (dialogs, startLoad) => ({
    type: GET_DIALOGS,
    data: { dialogs, startLoad },
})
export const getMessagesCR = messages => ({ type: GET_MESSAGE, messages })
export const changeOnActiveCR = index => ({ type: CHANGE_ON_ACTIVE, index })
export const changeOnSavedCR = index => ({ type: CHANGE_ON_SAVED, index })
export const getPeiceDialogsCR = (dialogs, startLoad) => ({
    type: GET_DIALOGS_PIECE,
    data: { dialogs, startLoad },
})

// export const getUsersCR = ownersMessages => ({
//     type: GET_USERS,
//     ownersMessages,
// })

let initialState = {
    dialogs: [],
    dialogsActive: [],
    dialogsSaved: [],
    dialogsCompleted: [],
    messages: [],
    startLoad: 0,
    // ownersMessages: [],
}

const dialogsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_DIALOGS: {
            // debugger
            return {
                ...state,
                dialogs: [...action.data.dialogs],
                startLoad: action.data.startLoad,
            }
        }
        case GET_DIALOGS_PIECE: {
            return {
                ...state,
                dialogs: [...state.dialogs, ...action.data.dialogs],
                startLoad: action.data.startLoad,
            }
        }
        case GET_MESSAGE: {
            return {
                ...state,
                messages: [...action.messages],
            }
        }
        case CHANGE_ON_ACTIVE: {
            let newState = state.dialogs.filter((dialog, index) => {
                if (index === action.index) {
                    console.log(index, 'isActive')
                    dialog.isActive = true
                    dialog.isSaved = false
                    dialog.isCompleted = false
                    return dialog
                }
            })
            return {
                ...state,
                dialogsActive: [...state.dialogsActive, ...newState],
            }
        }
        case CHANGE_ON_SAVED: {
            let newState = state.dialogsActive.filter((dialog, index) => {
                if (index === action.index) {
                    console.log(index, 'isSaved')
                    dialog.isActive = false
                    dialog.isSaved = true
                    dialog.isCompleted = false
                    return dialog
                }
            })
            let newDialogsActive = state.dialogsActive.filter(dialog => {
                if (dialog.isActive) return dialog
            })
            return {
                ...state,
                dialogsActive: [...newDialogsActive],
                dialogsSaved: [...state.dialogsSaved, ...newState],
            }
        }
        // case GET_USERS: {
        //     return {
        //         ...state,
        //         ownersMessages: [...action.ownersMessages],
        //     }
        // }
        default:
            return state
    }
}

export default dialogsReducer
