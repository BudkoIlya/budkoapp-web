const ERROR = 'ERROR'
const SET_USERS = 'SET_USERS'
const SIGN_IN = 'SIGN_IN'
const SIGN_OUT = 'SIGN_OUT'

export const errSignCreator = err => ({ type: ERROR, err })
export const setUsersCreator = users => ({ type: SET_USERS, users })
export const signInCreator = user => ({ type: SIGN_IN, user })
export const signOutCreator = () => ({ type: SIGN_OUT })

let initialState = {
    users: [],
    status: false,
    err: '',
    token: null,
}

const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case ERROR: {
            return {
                ...state,
                status: false,
                err: action,
            }
        }
        case SET_USERS: {
            return { ...state, users: [...action.users] }
        }
        case SIGN_IN: {
            return {
                ...state,
                status: action.user.email,
                token: action.user.JWToken,
                err: '',
            }
        }
        case SIGN_OUT: {
            return {
                ...state,
                status: false,
                token: null,
            }
        }
        default:
            return state
    }
}

export default usersReducer
