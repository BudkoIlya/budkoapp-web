import React, { Component } from 'react'
import ActivePage from './activePage'
import { connect } from 'react-redux'
import { signOutCreator } from '../../reducers/users-reducer'
import { changeOnSavedCR } from '../../reducers/dialogs-reducer'
import { DateTime } from 'luxon'

class ActivePageContainer extends Component {
    constructor(props) {
        super(props)
    }
    setSaved = (id, index) => {
        fetch('http://localhost:3001/isSaved', {
            method: 'POST',
            mode: 'cors',
            cache: 'default',
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ id, index }),
        })
            .then(res => res.json())
            .then(res => {
                this.props.changeOnSaved(index)
            })
    }
    setTime = sentTime => {
        const time = DateTime.fromISO(sentTime)
        // высчитываем разницу между данным временеи и временем отправки
        const i = DateTime.local().diff(time, ['hours', 'minutes'])

        if (time.plus({ days: 1 }) < DateTime.local())
            return time.toLocaleString(DateTime.DATE_SHORT)
        else {
            console.log(i)
            return `${i.toObject().hours} часа назад`
        }
    }
    render() {
        return (
            <ActivePage
                {...this.props}
                setSaved={this.setSaved}
                setTime={this.setTime}
            />
        )
    }
}

let mapStateToProps = ({ users, dialogs }) => ({
    dialogs,
    users,
})
let mapDispatchToProps = dispatch => {
    return {
        signOut: () => {
            dispatch(signOutCreator())
        },
        changeOnSaved: index => {
            dispatch(changeOnSavedCR(index))
        },
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ActivePageContainer)
