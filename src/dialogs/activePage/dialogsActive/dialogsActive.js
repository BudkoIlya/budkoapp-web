import React, { Component } from 'react'
import style from './dialogsActive.module.css'

class DialogsActive extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        // debugger
        return (
            <div className="d-flex justify-content-around border border-dark mb-2 p-2">
                <div className="p-2 d-flex align-items-center">
                    {this.props.client}
                </div>
                <div className="p-2 flex-grow-1">{this.props.description}</div>
                <div>
                    <p>Продолжить</p>
                    <p>{this.props.setTime(this.props.sentTime)}</p>
                    <p
                        className={style.save}
                        onClick={() =>
                            this.props.setSaved(this.props.id, this.props.index)
                        }
                    >
                        Сохранить
                    </p>
                </div>
            </div>
        )
    }
}

export default DialogsActive
