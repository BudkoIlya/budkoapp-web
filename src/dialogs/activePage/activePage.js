import React, { Component } from 'react'
import ButtonExit from '../buttonExit/buttonExit'
import style from '../getStartPage/startPage.module.css'
import SearchContainer from '../search/searchContainer'
import DialogsActive from '../activePage/dialogsActive/dialogsActive'

class ActivePage extends Component {
    constructor(props) {
        super(props)
    }
    componentDidMount() {}
    render() {
        return (
            <div className={`d-flex flex-column ${style.activePage}`}>
                <div className="d-flex">
                    <div className="d-flex align-items-center">
                        {this.props.users.status}
                    </div>
                    <ButtonExit signOut={this.props.signOut} />
                </div>
                <div className="d-flex">
                    {/*здесь поиск будет по стейту*/}
                    <label className="mr-3 d-flex align-items-center">
                        Поиск:
                    </label>
                    <SearchContainer />
                </div>
                <div className={style.messages}>
                    {this.props.dialogs.dialogsActive.map((dialog, index) => {
                        if (dialog && dialog.isActive === true)
                            return (
                                <DialogsActive
                                    key={index}
                                    index={index}
                                    {...dialog}
                                    setSaved={this.props.setSaved}
                                    setTime={this.props.setTime}
                                />
                            )
                    })}
                </div>
            </div>
        )
    }
}

export default ActivePage
