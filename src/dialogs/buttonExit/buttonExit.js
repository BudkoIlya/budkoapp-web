import React from 'react'
import { Button } from 'reactstrap'
import { Link } from 'react-router-dom'

const ButtonExit = props => {
    return (
        <div>
            <Button color="dark" className="m-3">
                <Link
                    to="/"
                    className="text-white text-decoration-none"
                    onClick={props.signOut}
                >
                    Выход
                </Link>
            </Button>
        </div>
    )
}

export default ButtonExit
