import React, { Component } from 'react'
import ButtonExit from '../buttonExit/buttonExit'
import style from '../getStartPage/startPage.module.css'
import SearchContainer from '../search/searchContainer'
import DalogsSaved from './dialogsSaved/dialogsSaved'

class ActivePage extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <div className={`d-flex flex-column ${style.activePage}`}>
                <div className="d-flex">
                    <div className="d-flex align-items-center">
                        {this.props.users.status}
                    </div>
                    <ButtonExit signOut={this.props.signOut} />
                </div>
                <div className="d-flex">
                    {/*здесь поиск будет по стейту*/}
                    <label className="mr-3 d-flex align-items-center">
                        Поиск:
                    </label>
                    <SearchContainer />
                </div>
                <div className={style.messages}>
                    {this.props.dialogs.dialogsSaved.map((dialog, index) => {
                        if (dialog && dialog.isSaved === true)
                            return (
                                <DalogsSaved
                                    key={index}
                                    {...dialog}
                                    setTime={this.props.setTime}
                                />
                            )
                    })}
                </div>
            </div>
        )
    }
}

export default ActivePage
