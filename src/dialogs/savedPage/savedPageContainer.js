import React, { Component } from 'react'
import SavedPage from './savedPage'
import { connect } from 'react-redux'
import { signOutCreator } from '../../reducers/users-reducer'
import { DateTime } from 'luxon'

class ActivePageContainer extends Component {
    constructor(props) {
        super(props)
    }
    setTime = sentTime => {
        const time = DateTime.fromISO(sentTime)
        // высчитываем разницу между данным временеи и временем отправки
        const i = DateTime.local().diff(time, ['hours', 'minutes'])

        if (time.plus({ days: 1 }) < DateTime.local())
            return time.toLocaleString(DateTime.DATE_SHORT)
        else {
            console.log(i)
            return `${i.toObject().hours} часа назад`
        }
    }

    render() {
        return <SavedPage {...this.props} setTime={this.setTime} />
    }
}

let mapStateToProps = ({ users, dialogs }) => ({
    dialogs,
    users,
})
let mapDispatchToProps = dispatch => {
    return {
        signOut: () => {
            dispatch(signOutCreator())
        },
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ActivePageContainer)
