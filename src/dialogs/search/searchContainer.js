import React, { Component } from 'react'
import Search from './search'
import {
    getDialogsCR,
    getMessagesCR,
    // getUsersCR,
} from '../../reducers/dialogs-reducer'
import { connect } from 'react-redux'
import axios from 'axios'

class SearchContainer extends Component {
    // Поиск диалогов и сообщений
    // search = event => {
    //     // console.log(111, event.target.value)
    //     let value = { term: event.target.value.toLowerCase() }
    //     // console.log(222, 'term', value.term)
    //     // console.log(333, JSON.stringify(value))
    //     if (value.term.length) {
    //         // console.log(444, 'sent')
    //         fetch('http://localhost:3001/search', {
    //             method: 'POST',
    //             mode: 'cors',
    //             cache: 'default',
    //             headers: {
    //                 'Access-Control-Allow-Origin': '*',
    //                 'Content-Type': 'application/json',
    //             },
    //             body: JSON.stringify(value),
    //         })
    //             .then(res => res.json())
    //             .then(res => {
    //                 // debugger
    //                 this.props.getDialogs(res.dialogs)
    //                 this.props.getMessages(res.personMessages)
    //                 // this.props.getUsers(res.ownersMessages)
    //                 console.log('dialogs', res.dialogs)
    //                 console.log('messages', res.personMessages)
    //             })
    //     }
    // }
    // Поиск диалогов, его описания
    search = event => {
        let value = { term: event.target.value.toLowerCase() }
        if (value.term.length) {
            // console.log(444, 'sent')
            fetch('http://localhost:3001/search/dialogs', {
                method: 'POST',
                mode: 'cors',
                cache: 'default',
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(value),
            })
                .then(res => res.json())
                .then(res => {
                    console.log(res)
                    this.props.getDialogs(res)
                })
        } else {
            axios
                .post('http://localhost:3001/search/dialogs/all')
                .then(res => this.props.getDialogs(res.data))
        }
    }

    render() {
        return <Search search={this.search} />
    }
}

let mapStateToProps = ({ dialogs }) => ({ dialogs })
let mapDispatchToProps = dispatch => {
    return {
        getDialogs: dialogs => {
            dispatch(getDialogsCR(dialogs))
        },
        getMessages: messages => {
            dispatch(getMessagesCR(messages))
        },
        // getUsers: ownersMessages => {
        //     dispatch(getUsersCR(ownersMessages))
        // },
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SearchContainer)
