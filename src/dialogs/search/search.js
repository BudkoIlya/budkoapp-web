import React from 'react'
import { Input } from 'reactstrap'
import { Debounce } from 'react-throttle'

const Search = props => {
    return (
        // если следующее нажатие на кнопку происходит в интервале 400 мс то отправка не срабатывает
        <Debounce time="400" handler="onChange">
            <Input
                className="mb-2"
                type="text"
                placeholder="Search"
                onChange={props.search}
            />
        </Debounce>
    )
}

export default Search
