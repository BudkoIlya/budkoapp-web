import React from 'react'
import { withRouter, Link, Switch, Route } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import MainPage from './mainPage/mainPage'
import style from './dialogs.module.css'
import StartPageContainer from './getStartPage/startPageContainer'
import ActivePageContainer from './activePage/activePageContainer'
import SavedPageContainer from './savedPage/savedPageContainer'

const Dialogs = props => {
    return (
        <div>
            <div className="d-flex">
                <div className={`d-flex flex-column ${style.menu} p-2`}>
                    <Link to="/dialogs">Начальная страница</Link>
                    <Link to="/dialogs/getStartPage">Приступить</Link>
                    <Link to="/dialogs/activePage">Активные</Link>
                    <Link to="/dialogs/savedPage">Сохранненые</Link>
                    {/*<Link>Завершенные</Link>*/}
                </div>
                <Switch className={style.messages}>
                    {/*Главная страница*/}
                    <Route
                        path="/dialogs"
                        exact
                        render={() => (
                            <MainPage
                                signOut={props.signOut}
                                status={props.status}
                            />
                        )}
                    />
                    {/*Активные диалоги*/}
                    <Route
                        path="/dialogs/getStartPage"
                        component={StartPageContainer}
                    />
                    <Route
                        path="/dialogs/activePage"
                        component={ActivePageContainer}
                    />
                    <Route
                        path="/dialogs/savedPage"
                        component={SavedPageContainer}
                    />
                </Switch>
            </div>
            <ToastContainer
                position="top-center"
                autoClose={4000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnVisibilityChange
                draggable
                pauseOnHover
            />
        </div>
    )
}

export default withRouter(Dialogs)
