import React, { Component } from 'react'
import ButtonExit from '../buttonExit/buttonExit'
import style from './mainPage.module.css'

class MainPage extends Component {
    render() {
        return (
            <div className={`d-flex flex-column ${style.mainPage}`}>
                <div className="d-flex">
                    <div className="d-flex align-items-center">
                        {this.props.status}
                    </div>
                    <ButtonExit signOut={this.props.signOut} />
                </div>

                <div className={style.mainPahe__start}>Начальная страница</div>
            </div>
        )
    }
}

export default MainPage
