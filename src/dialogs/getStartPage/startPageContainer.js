import React, { Component } from 'react'
import StartPage from './startPage'
import { signOutCreator } from '../../reducers/users-reducer'
import {
    changeOnActiveCR,
    getDialogsCR,
    getPeiceDialogsCR,
} from '../../reducers/dialogs-reducer'
import { connect } from 'react-redux'
import axios from 'axios'
import { DateTime } from 'luxon'

class StartPageContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            hasMore: true,
        }
    }
    componentWillMount() {
        if (!this.props.dialogs.startLoad) {
            axios({
                method: 'post',
                url: 'http://localhost:3001/get/dialogs/',
                data: {
                    start: 0,
                    offset: 6,
                },
            }).then(res => {
                console.log('7 диалогов', res.data)
                this.props.getDialogs(res.data, 6)
            })
        }
    }
    getPieceDialogs = () => {
        if (this.props.dialogs.startLoad > 5) {
            axios({
                method: 'post',
                url: 'http://localhost:3001/get/dialogs/',
                data: {
                    start: this.props.dialogs.startLoad,
                    offset: 3,
                },
            }).then(res => {
                if (res.data.length) {
                    this.props.getPeiceDialogs(
                        res.data,
                        this.props.dialogs.dialogs.length + 3
                    )
                } else {
                    this.setState({
                        hasMore: false,
                    })
                }
            })
        }
    }
    setActive = (id, index) => {
        fetch('http://localhost:3001/isActive', {
            method: 'POST',
            mode: 'cors',
            cache: 'default',
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ id, index }),
        })
            .then(res => res.json())
            .then(res => {
                this.props.changeOnActive(index)
            })
    }
    setTime = sentTime => {
        const time = DateTime.fromISO(sentTime)
        // высчитываем разницу между данным временеи и временем отправки
        const i = DateTime.local().diff(time, ['hours', 'minutes'])

        if (time.plus({ days: 1 }) < DateTime.local())
            return time.toLocaleString(DateTime.DATE_SHORT)
        else {
            return `${i.toObject().hours} часа назад`
        }
    }
    render() {
        return (
            <StartPage
                {...this.props}
                setActive={this.setActive}
                setTime={this.setTime}
                getPieceDialogs={this.getPieceDialogs}
                state={this.state}
            />
        )
    }
}

let mapStateToProps = ({ users, dialogs }) => ({
    dialogs,
    users,
})
let mapDispatchToProps = dispatch => {
    return {
        signOut: () => {
            dispatch(signOutCreator())
        },
        changeOnActive: index => {
            dispatch(changeOnActiveCR(index))
        },
        getDialogs: (dialogs, startLoad) => {
            dispatch(getDialogsCR(dialogs, startLoad))
        },
        getPeiceDialogs: (dialogs, startLoad) => {
            dispatch(getPeiceDialogsCR(dialogs, startLoad))
        },
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(StartPageContainer)
