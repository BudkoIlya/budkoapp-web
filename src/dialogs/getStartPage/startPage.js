import React, { Component } from 'react'
import ButtonExit from '../buttonExit/buttonExit'
import style from './startPage.module.css'
import SearchContainer from '../search/searchContainer'
import DialogsStart from './dialogsStart/dialogsStart'
import InfiniteScroll from 'react-infinite-scroller'
import axios from 'axios'

class StartPage extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <div className={`d-flex flex-column ${style.activePage}`}>
                <div className="d-flex">
                    <div className="d-flex align-items-center">
                        {this.props.users.status}
                    </div>
                    <ButtonExit signOut={this.props.signOut} />
                </div>
                <div className="d-flex justify-content-between w-100">
                    <div className="d-flex align-items-center mb-2">
                        {`Клиентов в очереди: ${
                            this.props.dialogs.dialogs.filter(dialog => {
                                if (
                                    !dialog.isActive &&
                                    !dialog.isSaved &&
                                    !dialog.isCompleted
                                ) {
                                    return dialog
                                }
                            }).length
                        }`}
                    </div>
                    <div className="d-flex ">
                        <label className="mr-3 d-flex align-items-center">
                            Поиск:
                        </label>
                        <SearchContainer />
                    </div>
                </div>
                <div className={style.messages}>
                    <InfiniteScroll
                        pageStart={0}
                        loadMore={this.props.getPieceDialogs}
                        hasMore={this.props.state.hasMore}
                        loader={
                            <div className="loader" key={0}>
                                Loading ...
                            </div>
                        }
                        useWindow={false}
                    >
                        {this.props.dialogs.dialogs.map((dialog, index) => {
                            if (
                                !dialog.isActive &&
                                !dialog.isSaved &&
                                !dialog.isCompleted
                            ) {
                                return (
                                    // передаю индекс чтобы найти по нему в стэйте в масиве объект
                                    <DialogsStart
                                        key={index}
                                        index={index}
                                        {...dialog}
                                        setActive={this.props.setActive}
                                        setTime={this.props.setTime}
                                    />
                                )
                            }
                        })}
                    </InfiniteScroll>
                </div>
            </div>
        )
    }
}

export default StartPage
