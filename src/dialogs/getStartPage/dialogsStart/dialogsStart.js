import React, { Component } from 'react'
// import Moment from 'react-moment'
import style from './dialogsStart.module.css'

class DialogsStart extends Component {
    render() {
        // debugger
        return (
            <div className="d-flex justify-content-around border border-dark mb-2 p-2">
                <div className="p-2 d-flex align-items-center">
                    {this.props.client}
                </div>
                <div className="p-2 flex-grow-1">{this.props.description}</div>
                <div>
                    <p>{this.props.setTime(this.props.sentTime)}</p>
                    <p
                        className={style.active}
                        onClick={() =>
                            this.props.setActive(
                                this.props.id,
                                this.props.index
                            )
                        }
                    >
                        Войти в диалог
                    </p>
                </div>
            </div>
        )
    }
}

export default DialogsStart
