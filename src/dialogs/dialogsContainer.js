import React, { Component } from 'react'
import { signOutCreator } from '../reducers/users-reducer'
import Dialogs from './dialogs'
import { connect } from 'react-redux'
import { toast } from 'react-toastify'
import { withRouter } from 'react-router-dom'

class DialogsContaier extends Component {
    constructor(props) {
        super(props)
        // debugger;
        this.state = {
            showNotification: false,
        }
    }
    notifySuccess = () => {
        toast.success('Вы вошли!')
        this.setState({ showNotification: false })
    }

    componentDidMount() {
        if (this.props.users.status !== false) this.notifySuccess()
        // распаковка из джисона юзера(из локалсторедж)
        let users = localStorage.getItem('persist:users')
        if (users) {
            let usersParse = JSON.parse(users).users
            let token = JSON.parse(usersParse).token
            if (token !== null) {
                fetch('http://localhost:3001/token', {
                    method: 'POST',
                    mode: 'cors',
                    headers: {
                        Accept: '*/*',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({ token: token }),
                })
                    .then(res => res.json())
                    .then(res => {
                        if (res.err) {
                            this.props.history.push('/')
                            this.props.signOut()
                        } else {
                            console.log('answer', res)
                        }
                    })
            }
        }
    }

    render() {
        return (
            <Dialogs signOut={this.props.signOut} status={this.props.status} />
        )
    }
}

let mapStateToProps = ({ users }) => ({
    users,
})
let mapDispatchToProps = dispatch => {
    return {
        signOut: () => {
            dispatch(signOutCreator())
        },
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withRouter(DialogsContaier))
